package de.ohmhs.efi.bme5.timestretch.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract super class for all {@link Repository} implementations that holds a reference to an JPA
 * entityManager.
 * @param <M>
 * @param <I>
 */
public abstract class AbstractRepository<M,I> implements Repository<M,I> {
    protected EntityManager entityManager;
    protected final Class<M> entityClass;

    public AbstractRepository() {
        entityClass = (Class<M>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    /**
     * this method uses the <a href="https://www.baeldung.com/hibernate-criteria-queries">JPA Criteria API</a>
     * it is a object oriented way to build complex queries while using OOP style control mechanisms
     * instead of error prone string manipulations. its strength comes with the cost of complexity in design
     *
     * @param params a map with a string key and a object value where the attributes are filtered by attribute in the key and the equality to the value given by the map's value
     * @return the list of entities matchting the given filter criteria
     */
    @Override
    public List<M> findWithParams(Map<String, Object> params) {
        if(params == null) {
            params = new HashMap<>();
        }
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<M> q = cb.createQuery(entityClass);
        Root<M> p = q.from(entityClass);
        List<Predicate> predicates = new ArrayList<>();
        params.forEach((k,v) -> {
            predicates.add(cb.equal(p.get(k), v));
        });
        q.select(p);
        if(predicates.size() > 0) {
            q.where(predicates.toArray(new Predicate[0]));
        }
        TypedQuery<M> query = entityManager.createQuery(q);
        return query.getResultList();
    }

    @Override
    public List<M> findAll() {
        return findWithParams(null);
    }

    @Override
    public M findOne(I primaryKey) {
        return entityManager.find(entityClass, primaryKey);
    }

    @Override
    public M save(M entity) {
        return entityManager.merge(entity);
    }
}
