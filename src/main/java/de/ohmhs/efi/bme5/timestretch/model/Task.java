package de.ohmhs.efi.bme5.timestretch.model;

import jakarta.persistence.*;

/**
 * JPA Entity that stores information of a specific task. A task is a particular unit of work
 * under which one or more {@link Timer} instances can be saved that make up a worklog
 */
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TASK_GEN")
    @SequenceGenerator(name = "TASK_GEN", sequenceName = "TASK_SEQ", allocationSize = 1)
    private Long id;

    @Column(length = 120, unique = true, nullable = false)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
