package de.ohmhs.efi.bme5.timestretch.dao;

import de.ohmhs.efi.bme5.timestretch.model.Task;

import java.util.List;

/**
 * {@link Repository} implementation that takes care of persistence operations with {@link Task} entity instances
 *
 * @see de.ohmhs.efi.bme5.timestretch.dao.Repository
 * @see de.ohmhs.efi.bme5.timestretch.dao.AbstractRepository
 */
public class TaskRepository extends AbstractRepository<Task, Long> {
    public List<Task> getRunningTimerTasks() {
        return entityManager.createQuery("select t.task from Timer t where t.end is null", Task.class).getResultList();
    }
}
