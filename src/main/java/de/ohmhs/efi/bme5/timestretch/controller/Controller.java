package de.ohmhs.efi.bme5.timestretch.controller;

import de.ohmhs.efi.bme5.timestretch.dao.HibernateEngine;
import de.ohmhs.efi.bme5.timestretch.dao.Repository;
import de.ohmhs.efi.bme5.timestretch.model.Task;
import de.ohmhs.efi.bme5.timestretch.model.Timer;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import jakarta.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Central Controller class, that implements all {@link Handler} functional interfaces
 */
public class Controller {

    private static final Logger LOG = LoggerFactory.getLogger(Controller.class);

    private final Repository<Task, Long> taskRepo;
    private final Repository<Timer, Long> timerRepo;

    /**
     * Constructor that injects all needed repositories to be used for DB Operations
     * @param taskRepository the {@link Repository} instance responsible for {@link Task} persistence.
     * @param timerRepository the {@link Repository} instance responsible for {@link Timer} persistence.
     */
    public Controller(Repository<Task, Long> taskRepository, Repository<Timer, Long> timerRepository) {
        this.taskRepo = taskRepository;
        this.timerRepo = timerRepository;
    }

    /**
     * registers all handler methods at specific context paths and HTTP methods
     * @param app the {@link Javalin} instance to which the {@link Handler} instances are to be registered
     */
    public void registerEndpoints(Javalin app) {
        app.get("/", this::getEntryPoint);
        app.get("/tasks", this::getTaskList);
        app.get("/tasks/new", this::getTaskAdd);
        app.post("/tasks/new", this::postTaskAdd);
        app.get("/tasks/{id}", this::getTask);
        app.get("/tasks/{id}/startTimer", this::getStartTimer);
        app.get("/tasks/{id}/stopTimer", this::getStopTimer);
    }

    /**
     * handles GET requests for path "/"
     * redirects directly to a task view, if there is a timer record without end date
     * or to the list of timers when there is no timer record with end date unset (NULL)
     * if there are more timers without end a server error is returned with a message
     * @param ctx {@link Context} instance
     */
    public void getEntryPoint(Context ctx)  {
        EntityManager em =  HibernateEngine.getInstance().getEntityManager();
        List<Task> actTimers =  em.createQuery("select t.task from Timer t where t.end is null", Task.class).getResultList();
        em.close();
        switch (actTimers.size()) {
            case 0 -> ctx.redirect("/tasks");
            case 1 -> ctx.redirect("/tasks/" + actTimers.get(0).getId());
            default -> ctx.status(500).result("check database ... more than one timers are active");
        }
    };

    /**
     * handles GET requests for path "/tasks"
     * @param ctx {@link Context} instance
     */
    public void getTaskList(Context ctx) {
        ctx.render("taskList.html", Map.of("tasks", taskRepo.findAll()));
    }

    /**
     * handles GET requests for path "/tasks/{id}"
     * @param ctx {@link Context} instance
     */
    public void getTask(Context ctx) {
        Long taskId = ctx.pathParamAsClass("id", Long.class).get();
        List<Timer> timers =  timerRepo.findWithParams(Map.of("task",taskRepo.findOne(taskId)));
        boolean isTimerActive = timers.stream().anyMatch(t -> t.getEnd() == null);
        long mins = timers.stream()
                .filter(t -> t.getEnd() != null)
                .map(t -> ChronoUnit.MINUTES.between(t.getStart().toInstant(), t.getEnd().toInstant()))
                .mapToLong(Long::longValue)
                .sum();
        LOG.info("timers in minutes: "+ mins);
        Duration dur = Duration.ofMinutes(mins);
        Map<String,Integer> durRead = Map.of("min",dur.toMinutesPart(),"hour",dur.toHoursPart());
        ctx.render("taskList.html", Map.of(
                "tasks", taskRepo.findAll(),
                "taskId", taskId,
                "timers", timers,
                "sum", durRead,
                "isTimerActive", isTimerActive
        ));
    }

    /**
     * handles GET requests for path "/tasks/new" (form view)
     * @param ctx {@link Context} instance
     */
    public void getTaskAdd(Context ctx) {
        ctx.render("taskNew.html");
    }

    /**
     * handles POST requests for path "/tasks/new" (form submit)
     * @param ctx {@link Context} instance
     */
    public void postTaskAdd(Context ctx) {
        Task task = new Task();
        task.setName(ctx.formParam("taskname"));
        taskRepo.save(task);
        ctx.redirect("/tasks");
    }

    /**
     * handles GET requests for path "/tasks/{id}/startTimer"
     * @param ctx {@link Context} instance
     */
    public void getStartTimer(Context ctx) {
        Task task = taskRepo.findOne(ctx.pathParamAsClass("id",Long.class).get());
        Timer t = new Timer();
        t.setTask(task);
        timerRepo.save(t);
        ctx.redirect("/tasks/"+task.getId().toString());
    }

    /**
     * handles GET requests for path "/tasks/{id}/stopTimer"
     * @param ctx {@link Context} instance
     */
    public void getStopTimer(Context ctx) {
        Long taskId = ctx.pathParamAsClass("id", Long.class).get();
        List<Timer> timers =  timerRepo.findWithParams(Map.of("task",taskRepo.findOne(taskId)));
        timers.stream().filter(t -> t.getEnd() == null).forEach(t -> {
            t.setEnd(new Date());
            timerRepo.save(t);
        });
        ctx.redirect("/tasks/"+taskId);
    }
}
