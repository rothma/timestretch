package de.ohmhs.efi.bme5.timestretch;

import de.ohmhs.efi.bme5.timestretch.build.AppBuilder;
import de.ohmhs.efi.bme5.timestretch.build.AppFactory;
import io.javalin.http.staticfiles.Location;

/**
 * Main Class that bootstraps the entire Application.
 */
public class Main {
    /**
     * Main class of this app that will be called to bootstrap the application.
     * It's implementation uses an AppBuilder and Factory instance to create the
     * Object graph and build the app structure
     *
     * @param args progamm arguments (e.g. java -jar timestretch-full.jar argument1 argument2)
     */
    public static void main(String[] args) {
        AppBuilder builder = AppBuilder
                .getInstance()
                .withFactory(new AppFactory())
                .addStaticFiles("static", Location.CLASSPATH);
        builder.build();
    }
}
