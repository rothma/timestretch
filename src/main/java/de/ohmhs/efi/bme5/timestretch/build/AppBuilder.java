package de.ohmhs.efi.bme5.timestretch.build;

import de.ohmhs.efi.bme5.timestretch.controller.Controller;
import de.ohmhs.efi.bme5.timestretch.dao.Repository;
import de.ohmhs.efi.bme5.timestretch.model.Task;
import de.ohmhs.efi.bme5.timestretch.model.Timer;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.rendering.template.JavalinThymeleaf;

import java.util.HashMap;
import java.util.Map;

/**
 * AppBuilder is responsible for corrdination of building the object graph for the overall application
 * it uses a {@link Factory} implementation for object creation and associates objects like Repositories
 * and the Controller as well as the {@link Javalin} application instance.
 * static file configuration and the definition of the concrete Factory implementation can be given by the
 * caller. Via method-chaining (AppBuilder.getInstance().withFactory(FOO).build()) the resulting preconfigured
 * Javalin App.
 */
public final class AppBuilder {

    private static AppBuilder _INSTANCE = null;
    private Factory factory;
    private final Map<String, Location> staticFiles = new HashMap<>();

    private AppBuilder() {}

    /**
     * The <a href="https://en.wikipedia.org/wiki/Singleton_pattern">Singleton</a> method that ensures that only one Instance of AppBuilder is returned in the whole
     * app lifecycle
     * @return the single object Instance managed by this Builder
     */
    public static AppBuilder getInstance() {
        if(_INSTANCE == null) {
            _INSTANCE = new AppBuilder();
        }
        return _INSTANCE;
    }

    /**
     * configures a @{@link Factory} instance to be called for creating the Controller or Repository instances
     * @param factory a factory implementation used by this builder
     * @return the AppBuilder itself suitable for method-chaining
     */
    public AppBuilder withFactory(Factory factory) {
        this.factory = factory;
        return this;
    }

    /**
     * Fassade method that delegates the static file configuration
     * @param path the path that is seen as static file path
     * @param location the location (classpath or filesystem)
     * @return the AppBuilder itself suitable for method-chaining
     */
    public AppBuilder addStaticFiles(String path, Location location) {
        this.staticFiles.put(path,location);
        return this;
    }

    /**
     * building the overall object-graph of Timstretch Application.
     * @return the {@link Javalin} instance that is fully configured and started.
     */
    public Javalin build() {
        Repository timerRepo = factory.createRepository(Timer.class);
        Repository taskRepo = factory.createRepository(Task.class);
        Controller controller = factory.createController(taskRepo, timerRepo);
        Javalin app = Javalin.create(c -> {
            staticFiles.forEach(c.staticFiles::add);
            c.fileRenderer(new JavalinThymeleaf());
        });
        controller.registerEndpoints(app);
        app.start();
        return app;
    }
}
