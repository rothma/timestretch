Beispielanwendung timestretch
=======================
Dieses Repository beinhaltet den Quelltext für die Übung aus der Lehrveranstaltung
Internetprogrammierung an der Technischen Hochschule Nürnberg.

Die Webanwendung ist ein kleines Tool zum Nachverfolgen von Zeit, die für gewissen Tätigkeiten aufgewendet wurde.

Dependencies
---------------
Die Anwendung setzt für das Kompilieren und Ausführen das Vorhandensein
eines Java SDK's (>= Version 17) voraus. Ich empfehle die Installation des
[Adoptium OpenJDK](https://adoptium.net/), da diese Downloads für alle gängigen
Betriebssysteme gepflegt und mit Installroutinen ausgeliefert werden.


Die Anwendung benutzt folgende Bibliotheken (Frameworks):
* [Javalin](https://javalin.io) als Web-Microframework mit embedded Jetty Servlet-Container
  und einfacher API für serverseitige Programmierung von HTTP und Websockets
* [Thymeleaf](https://www.thymeleaf.org) als Templating-Engine zur Erstellung dynamisscher
  serverseitiger HTML-Ausgaben
* [Postgres-JDBC-Treiber](https://jdbc.postgresql.org) für die Konnektivität zu
  Postgres Instanzen in Java-Anwendungen via JDBC.
* [Hibernate ORM](https://hibernate.org/orm/) ein bekannter Open-Source Objekt-Relationale Mapping
  Engine

Datenbank
---------
Voraussetzung für den Betrieb ist eine installierte Postgres instanz, die mit einer
eigenen Datenbank und einem Benutzer darin mit ReadWrite Berechtigungen vorbereitet
worden ist. Bei einer frisch installierten Postgres-Datenbank kann via Postgres Client-Shell
eine Verbindung aufgebaut werden:

```
psql
```

Damit ist man mit der Masterdatenbank "postgres" verbunden und die weiteren Aktionen
finden per SQL DDL Statements statt. Zuerst wird eine eigene Datenbank 'timestretch' angelegt':

`create database timestretch;`

Danach ein Datenbankbenutzer mit dazugehörigem Passwort:

`create user timestretch password 'timestretch';`

Nun wird die neue Datenbank dem Benutzer 'timestretch' als Eigentümer zugewiesen. Damit
hat dieser User umfangreiche Berechtigungen auf dieser Datenbank.

`alter database timestretch owner to timestretch;`

Damit auch aus dem Netzerk eine Verbindung aufgebaut werden kann muss dem User 'timestretch'
explizit erlaubt werden, dies für die Datenbank 'timestretch' zu tun

grant connect on database timestretch to timestretch;`

Zuletzt wird innerhalb der Datenbank 'timestretch' ein Schema mit gleichem Namen angelegt
und auch dem richtigen Beutzer als Eigentümer zugewiesen:

```
create schema 'timestretch';
alter schema timestretch owner to timestretch;
```

Nun kann die Postgres Client Shell per '\q' verlassen werden.


Bauen und Ausführung der Anwendung
-------------------
Da dieses Projekt [maven](https://maven.apache.org) verwendet, ist eine Installation von
Apache maven (standalone oder in der IDE integriert) notwendig. im Wurzelverzeichnisses
dieses Projektes genügt zum Bauen folgender Befehl

```
mvn package
```
Dadurch wird die Anwendung als Self-Contained-App gebaut und im
generierten Verzeichnis 'target' abgelegt.

Das Ausführen kann in der Kommandozeile erfolgen durch:
```
cd target
java -jar timestretch-full.jar
```
