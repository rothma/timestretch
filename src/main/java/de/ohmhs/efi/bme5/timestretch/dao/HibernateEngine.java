package de.ohmhs.efi.bme5.timestretch.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * HibernateEngine acts as a singleton base class that is just responsible for
 * creating a {@link EntityManagerFactory} instance usable by any {@link Repository} as a
 * shared reference.
 */
public class HibernateEngine {

    private static HibernateEngine _INSTANCE = null;
    private EntityManagerFactory emf;

    private HibernateEngine() {}

    /**
     * The <a href="https://en.wikipedia.org/wiki/Singleton_pattern">Singleton</a> method that ensures that only one Instance of HibernateEngine is returned in the whole
     * app lifecycle
     * @return the single object Instance managed by this Engine
     */
    public static HibernateEngine getInstance() {
        if(_INSTANCE == null) {
            _INSTANCE = new HibernateEngine();
            _INSTANCE.init();
        }
        return _INSTANCE;
    }

    /**
     * initializes the EntityManagerFctory used throughout the whole app lifecycle
     */
    private void init() {
        this.emf = Persistence.createEntityManagerFactory("timestretch");
    }

    /**
     * returns a new instance of {@link EntityManager} when called
     * @return a fresh {@link EntityManager} for usage in repos
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * operation to be called when thie application is stopped gracefully to release resources blocked
     * by underlying {@link EntityManagerFactory}.
     */
    public void close() {
        emf.close();
    }
}
