package de.ohmhs.efi.bme5.timestretch.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;

import java.util.List;
import java.util.Map;

/**
 * This class acts as a <a href="https://en.wikipedia.org/wiki/Decorator_pattern">Decorator</a> for
 * {@link Repository} implementations. It takes care of maintaining the lifecycle of the {@link EntityManager}
 * and {@link EntityTransaction} in methods that are supposed to change data in the underlying Database.
 * This way the repositry implementations can concentrate on working with the entityManager and entity
 * objects without having to care about maintaining states and transactions.
 *
 * It conforms to the {@link Repository} interface and delegates all its defined methods to the decorated
 * subclass of {@link AbstractRepository}.
 *
 * @param <M> the entity class type this Decorator handles with the coworking {@link AbstractRepository} delegate
 * @param <I> the primary key class of the entity (M)
 */
public class HibernateRepositoryDecorator<M,I> implements Repository<M,I> {

    protected HibernateEngine engine;
    protected AbstractRepository<M,I> delegate;

    /**
     * Constructor for this Decorator.
     * @param engine the hibernate engine as a provider for a {@link EntityManagerFactory} instance
     * @param delegate a subclass of a {@link AbstractRepository}.
     */
    public HibernateRepositoryDecorator(HibernateEngine engine, AbstractRepository<M,I> delegate) {
        this.engine = engine;
        this.delegate = delegate;
    }

    @Override
    public List<M> findAll() {
        init(false);
        List<M> res = this.delegate.findAll();
        tearDown(null);
        return res;
    }

    @Override
    public List<M> findWithParams(Map<String, Object> params) {
        init(false);
        List<M> res = this.delegate.findWithParams(params);
        this.delegate.entityManager.close();
        this.delegate.entityManager = null;
        return res;

    }

    @Override
    public M findOne(I primaryKey) {
        init(false);
        M res = this.delegate.findOne(primaryKey);
        tearDown(null);
        return res;
    }

    @Override
    public M save(M entity) {
        EntityTransaction t = init(true);
        this.delegate.save(entity);
        tearDown(t);
        return entity;
    }

    /**
     * private method to be called first on all defined interface methods.
     * gets a new EntityManager and injects it to the delegate. In a transactional
     * context it also begins a transaction and gives it back to the caller
     * @param isTransactional whether the caller wants to manipulate data in a db transaction
     * @return an transaction method or null if 'isTransactional' was set to false
     */
    private EntityTransaction init(boolean isTransactional) {
        this.delegate.entityManager = engine.getEntityManager();
        if(isTransactional) {
            EntityTransaction t = this.delegate.entityManager.getTransaction();
            t.begin();
            return t;
        }
        return null;
    }

    /**
     * does the internal housekeeping after calling methods on entityManager by the delegate.
     * commits the db transaction if it is given and active and closes the entitymanager.
     * @param transaction the transaction used by the caller throughout its operations
     */
    private void tearDown(EntityTransaction transaction) {
        if(transaction != null && transaction.isActive()) {
            transaction.commit();
        }
        this.delegate.entityManager.close();
        this.delegate.entityManager = null;
    }


}
