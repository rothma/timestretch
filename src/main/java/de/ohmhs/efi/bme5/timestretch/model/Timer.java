package de.ohmhs.efi.bme5.timestretch.model;

import jakarta.persistence.*;

import java.util.Date;

/**
 * JPA Entity that stores information of a specific timer. A timer is a log of a start time
 * and end time associated to a particular {@link Task}. The sum of all timers of a task
 * are seen as total worklog
 */
@Entity
public class Timer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIMER_GEN")
    @SequenceGenerator(name = "TIMER_GEN", sequenceName = "TIMER_SEQ", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "task_id", nullable = false)
    private Task task;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_ts" , nullable = false)
    private Date start = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_ts")
    private Date end;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
