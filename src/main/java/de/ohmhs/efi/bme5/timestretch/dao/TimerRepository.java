package de.ohmhs.efi.bme5.timestretch.dao;

import de.ohmhs.efi.bme5.timestretch.model.Timer;

import java.util.List;


/**
 * {@link Repository} implementation that takes care of persistence operations with {@link Timer} entity instances
 *
 * @see de.ohmhs.efi.bme5.timestretch.dao.Repository
 * @see de.ohmhs.efi.bme5.timestretch.dao.AbstractRepository
 */
public class TimerRepository extends AbstractRepository<Timer,Long> {
}
