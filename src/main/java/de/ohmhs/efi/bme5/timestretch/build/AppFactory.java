package de.ohmhs.efi.bme5.timestretch.build;

import de.ohmhs.efi.bme5.timestretch.controller.Controller;
import de.ohmhs.efi.bme5.timestretch.dao.*;
import jakarta.persistence.Entity;

public final class AppFactory implements Factory {

    /**
     *
     * @param entityClass the class annotated with {@link Entity} for whch the rpos should be responsible for
     * @return the {@link Repository} instance that handles entities given by entityClass
     */
    @Override
    public Repository<?,?> createRepository(Class entityClass) {
        return switch (entityClass.getSimpleName()) {
            case "Timer" -> new HibernateRepositoryDecorator<>(HibernateEngine.getInstance(), new TimerRepository());
            case "Task" -> new HibernateRepositoryDecorator<>(HibernateEngine.getInstance(), new TaskRepository());
            default -> null;
        };
    }

    /**
     * the {@link Controller} instance for this application
     * @param taskRepository task repository used by controller
     * @param timerRepository timer repository used by controller
     * @return the controller instance
     */
    @Override
    public Controller createController(Repository taskRepository, Repository timerRepository) {
        return new Controller(taskRepository, timerRepository);
    }
}
