package de.ohmhs.efi.bme5.timestretch.dao;

import java.util.List;
import java.util.Map;

/**
 * This Interface defines the contract of a Repository according to the <a href="https://en.wikipedia.org/wiki/Data_access_object">DAO pattern.</a>
 * Every implementation is solely responsible for persistence operations on a specific entity class.
 * easy <a href="https://en.wikipedia.org/wiki/Create,_read,_update_and_delete">CRUD</a> operations are predefined.
 * @param <M> the entity class the repository is reponsible
 * @param <I> the primary key type associated with the given entity (for lookups based on primary key)
 */
public interface Repository<M,I> {
    /**
     * find all instances for the given entity
     * @return a list of all entities available in database
     */
    List<M> findAll();

    /**
     * finds all instances of the given entity that conform to the filter criterion given as parameter
     * @param params a map with a string key and a object value where the attributes are filtered by attribute in the key and the equality to the value given by the map's value
     * @return a list of entities that conform to the filter criterion
     */
    List<M> findWithParams(Map<String, Object> params);

    /**
     * find a specific entity by it's primary key
     * @param primaryKey the primary key used for lookup
     * @return the single entity
     */
    M findOne(I primaryKey);

    /**
     * synchronized the given entity instance with the database.
     * implementaors have to make sure that the action is corrdinated in a db transaction
     * @param entity the entity instance (managed or detached) that should be synchronized with db
     * @return the entity reference that is managed after merge operation in entity Manager
     */
    M save(M entity);
}
