package de.ohmhs.efi.bme5.timestretch.build;

import de.ohmhs.efi.bme5.timestretch.controller.Controller;
import de.ohmhs.efi.bme5.timestretch.dao.HibernateEngine;
import de.ohmhs.efi.bme5.timestretch.dao.Repository;

/**
 * Interface that defines all methods for creating concrete {@link Repository} and {@link Controller}
 * instances. Implemetations are reponsible to provide the fitting object instance that conform to the
 * defined Interfaces
 */
public interface Factory {
    /**
     * creates repository for a given entity class
     * @param entityClass the class annotated with {@link javax.persistence.Entity} for whch the rpos should be responsible for
     * @return the {@link Repository} instance that handles entities given by entityClass
     */
    Repository createRepository(Class entityClass);

    /**
     * the {@link Controller} instance for this application
     * @param taskRepository task repository used by controller
     * @param timerRepository timer repository used by controller
     * @return the controller instance
     */
    Controller createController(Repository taskRepository, Repository timerRepository);
}
